﻿using System.Diagnostics;

namespace API
{
    public class Job
    {
        public Job(int id, int frame, EnumStatus status = EnumStatus.WAITING)
        {
            Id = id;
            Frame = frame;
            this.status = status;
        }

        public int Id { get; set; }
        public String Status => status.Convert();
        public int Frame { get; set; }

        public EnumStatus status;
        public Process process;
    }
}
